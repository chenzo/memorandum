#!/usr/bin/python3

###
# Author: Vincent Lucas <vincent.lucas@gmail.com>
###

from memorandum.cmds import cmds
import sys

if __name__ == "__main__":
    cmds()
    sys.exit(0)

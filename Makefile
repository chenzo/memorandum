all: run

run:
	python3 -m memorandum

test: 
	python3 -m unittest discover -v memorandum/test/

coverage:
	python3 -m coverage run -m unittest discover memorandum/test/
	python3 -m coverage report -m --include "memorandum/*" --omit "memorandum/test/*"

bdist:
	python3 setup.py bdist

sdist:
	python3 setup.py sdist

install:
	python3 setup.py install --user

install_root:
	su -c "python3 setup.py install"

uninstall:
	pip3 uninstall memorandum

pypi_register:
	python3 setup.py register

pypi_upload:
	python setup.py sdist upload
	# python setup.py sdist bdist_wininst upload

clean:
	rm -rf build
	rm -rf dist
	find . -name "*.pyc" | xargs rm

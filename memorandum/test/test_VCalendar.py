#!/usr/bin/python3

import unittest

from memorandum.VCalendar import VCalendar

###
# VCalendar unit tests.
###
class test_VCalendar(unittest.TestCase):

    ###
    # Prepare the CalendarCmds object
    ###
    def setUp(self):
        pass

    ###
    # Close the CalendarCmds object
    ###
    def tearDown(self):
        pass

    ###
    # Test to catch exception when parsing a bad event
    ###
    def test_parse_bad_event(self):
        filename = "memorandum/test/data/bad_vcalendar.ics"

        vcalendar = VCalendar()
        vcalendar.read(filename)
        uids = vcalendar.get_uids()

        # Since the parse failed, the number of events loaded is null.
        self.assertTrue(len(uids) == 0)

    ###
    # Test to catch exception when parsing a good event
    ###
    def test_parse_good_event(self):
        filename = "memorandum/test/data/good_vcalendar.ics"

        vcalendar = VCalendar()
        vcalendar.read(filename)
        uids = vcalendar.get_uids()

        self.assertTrue(len(uids) == 1)

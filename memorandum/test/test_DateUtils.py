#!/usr/bin/python3

import datetime
import unittest

from memorandum.DateUtils import DateUtils

###
# DateUtils unit tests.
###
class DateUtilsTests(unittest.TestCase):

    ###
    # Prepare the DateUtils object
    ###
    def setUp(self):
        pass

    ###
    # Close the DateUtils object
    ###
    def tearDown(self):
        pass


    ###
    # Test to get the datetime of the start of the day.
    ###
    def test_get_day_start(self):
        # Test with a given date
        day = DateUtils.get_datetime("20170901")
        start_day = DateUtils.get_day_start(0, day)
        res = DateUtils.get_caldav_string(start_day)
        self.assertEqual(res, "20170901T000000Z")

        # Test without a given date : today autocomputed.
        today = datetime.date.today()
        start_today = DateUtils.get_day_start(0, today)
        res_today = DateUtils.get_caldav_string(start_today)
        start_day = DateUtils.get_day_start()
        res_day = DateUtils.get_caldav_string(start_day)
        self.assertEqual(res_day, res_today)

    ###
    # Test to get the datetime of the end of the day.
    ###
    def test_get_day_end(self):
        # Test with a given date
        day = DateUtils.get_datetime("20170901")
        end_day = DateUtils.get_day_end(0, day)
        res = DateUtils.get_caldav_string(end_day)
        self.assertEqual(res, "20170901T235959Z")

        # Test without a given date : today autocomputed.
        today = datetime.date.today()
        end_today = DateUtils.get_day_end(0, today)
        res_today = DateUtils.get_caldav_string(end_today)
        end_day = DateUtils.get_day_end()
        res_day = DateUtils.get_caldav_string(end_day)
        self.assertEqual(res_day, res_today)

    ###
    # Test to get the date of the day starting the week.
    ###
    def test_get_weekday_start(self):
        day = DateUtils.get_datetime("20170627")
        start_day = DateUtils.get_weekday_start(day)
        res = DateUtils.get_date_string(start_day)
        self.assertEqual(res, "20170626")

    ###
    # Test to get the date of the day ending the week.
    ###
    def test_get_weekday_end(self):
        day = DateUtils.get_datetime("20170627")
        end_day = DateUtils.get_weekday_end(day)
        res = DateUtils.get_date_string(end_day)
        self.assertEqual(res, "20170702")

    ###
    # Test the get_datetime fonction and how it deals with different parameter
    # formats.
    ###
    def test_string_datetime_conversions(self):
        # Test with an incorrect date string
        day_str = "2017ABCD"
        day = DateUtils.get_datetime(day_str)
        self.assertEqual(day, None)

        # Test with a YYYYMMDD date string
        day_str = "20170831"
        day = DateUtils.get_datetime(day_str)
        self.assertEqual(DateUtils.get_date_string(day), day_str)

        # Test with a YYYYMMDDTHHMMSS date string
        day_str = "20170831T214654"
        day = DateUtils.get_datetime(day_str)
        self.assertEqual(
                DateUtils.get_caldav_string(day), "{0}Z".format(day_str))

        # Test with a YYYYMMDDTHHMMSSZ date string
        day_str = "20170831T214902Z"
        day = DateUtils.get_datetime(day_str)
        self.assertEqual(
                DateUtils.get_caldav_string(day), day_str)

    ###
    # Test conversion from date to datetime fonction.
    ###
    def test_date_to_datetime_conversions(self):
        # Test with a none date in order ro get today 00H00M00s.
        today = datetime.date.today()
        datetime_today = DateUtils.to_datetime(None)
        res_today = DateUtils.get_caldav_string(today)
        res_datetime_today = DateUtils.get_caldav_string(datetime_today)
        self.assertEqual(res_today, res_datetime_today)

        # Test with a given date
        test_res = "20170902T000000Z"
        day = datetime.date(2017, 9, 2)
        datetime_day = DateUtils.to_datetime(day)
        res_datetime_day = DateUtils.get_caldav_string(datetime_day)
        self.assertEqual(test_res, res_datetime_day)

#!/usr/bin/python3

import unittest
import os.path

from memorandum.CalendarCmds import CalendarCmds
from memorandum.Config import Config
from memorandum.DateUtils import DateUtils

###
# CalendarCmds unit tests.
###
class test_CalendarCmds(unittest.TestCase):

    ###
    # Prepare the CalendarCmds object
    ###
    def setUp(self):
        config = Config(conf_filename="memorandum_test.rc")
        self.calendarCmds = CalendarCmds(config.conf)

        # Delete all events
        events_all = self.calendarCmds.get_events(None, None)
        for event in events_all:
            self.calendarCmds.delete_event(event)

    ###
    # Close the CalendarCmds object
    ###
    def tearDown(self):
        self.calendarCmds.close()

    ###
    # Test to create one event
    ###
    def test_create_event(self):
        res = False
        summary = "test_create_event"

        date = "20170619"
        start_date = DateUtils.get_datetime(
                "{0}T080000Z".format(date))
        end_date = DateUtils.get_datetime(
                "{0}T100000Z".format(date))
        # Create one event
        uid = self.calendarCmds.create_event(
                summary,
                start_date,
                end_date)
        if(uid):
            res = True
            event = self.calendarCmds.get_event(uid)
            # Delete the event
            self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(res)

    ###
    # Test to create one recursive event
    ###
    def test_create_recursive_event(self):
        res = False
        summary = "test_create_recursive_event"

        date = "20170704"
        start_date = DateUtils.get_datetime(
                "{0}T210200Z".format(date))
        end_date = DateUtils.get_datetime(
                "{0}T213200Z".format(date))

        recurrence_frequency = "WEEKLY"
        recurrence_interval = 1
        recurrence_end_date_str = "20170711"
        recurrence_end_date = DateUtils.get_datetime(
                "{0}T235959Z".format(recurrence_end_date_str))

        # Create one recursive event
        uid = self.calendarCmds.create_event(
                summary,
                start_date,
                end_date,
                recurrence_frequency = recurrence_frequency,
                recurrence_interval = recurrence_interval,
                recurrence_end_date = recurrence_end_date)

        if(uid):
            res = True
            event = self.calendarCmds.get_event(uid)
            # Delete the event
            self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(res)

    ###
    # Test to delete one event
    ###
    def test_delete_event(self):
        res = False
        summary = "test_delete_event"

        date = "20170619"
        start_date = DateUtils.get_datetime(
                "{0}T080000Z".format(date))
        end_date = DateUtils.get_datetime(
                "{0}T100000Z".format(date))
        # Create one event
        uid = self.calendarCmds.create_event(
                summary,
                start_date,
                end_date)
        if(uid):
            event = self.calendarCmds.get_event(uid)
            # Delete the event
            self.calendarCmds.delete_event(event)
            if(self.calendarCmds.get_event(uid) == None):
                res = True

        # Return res
        self.assertTrue(res)

    ###
    # Test to get an unexisting event.
    ###
    def test_get_unexisting_event(self):
        uid = "unexisting_uid"
        event = self.calendarCmds.get_event(uid)

        # Return res
        self.assertTrue(event == None)

    ###
    # Test to get one event for 2017/06/19 date between 8h00 and 10h00.
    ###
    def test_get_events_day(self):
        res = False
        summary = "test_get_events_day"

        date = "20170619"
        start_date = DateUtils.get_datetime(
                "{0}T080000Z".format(date))
        end_date = DateUtils.get_datetime(
                "{0}T100000Z".format(date))
        # Create one event
        uid = self.calendarCmds.create_event(
                summary,
                start_date,
                end_date)
        if(uid):
            events = self.calendarCmds.get_events_day(date)
            for event in events:
                # Verify if the event correspond
                if(event.get_uid() == uid):
                    res = True
                    # Delete the event
                    self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(res)

    ###
    # Test to get one event today between 8h00 and 10h00.
    ###
    def test_get_events_today(self):
        res = False
        summary = "test_get_events_today"

        date = DateUtils.get_date_string(DateUtils.get_day_start(0))
        start_date = DateUtils.get_datetime(
                "{0}T080000Z".format(date))
        end_date = DateUtils.get_datetime(
                "{0}T100000Z".format(date))
        # Create one event
        uid = self.calendarCmds.create_event(
                summary,
                start_date,
                end_date)
        if(uid):
            events = self.calendarCmds.get_events_day()
            for event in events:
                # Verify if the event correspond
                if(event.get_uid() == uid):
                    res = True
                    # Delete the event
                    self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(res)

    ###
    # Test to get two events : this monday and this sunday between 8h00 and
    # 10h00.
    ###
    def test_get_events_this_week(self):
        start_event_found = False
        end_event_found = False

        summary1 = "test_get_events_this_week start"
        summary2 = "test_get_events_this_week end"

        date1 = DateUtils.get_date_string(DateUtils.get_weekday_start())
        start_date1 = DateUtils.get_datetime(
                "{0}T080000Z".format(date1))
        end_date1 = DateUtils.get_datetime(
                "{0}T100000Z".format(date1))
        date2 = DateUtils.get_date_string(DateUtils.get_weekday_end())
        start_date2 = DateUtils.get_datetime(
                "{0}T080000Z".format(date2))
        end_date2 = DateUtils.get_datetime(
                "{0}T100000Z".format(date2))
        # Create one event Monday and one Sunday
        uid1 = self.calendarCmds.create_event(
                summary1,
                start_date1,
                end_date1)
        uid2 = self.calendarCmds.create_event(
                summary2,
                start_date2,
                end_date2)
        if(uid1 and uid2):
            events = self.calendarCmds.get_events_week()
            for event in events:
                # Verify if the event correspond
                if(event.get_uid() == uid1):
                    start_event_found = True
                    # Delete the start event
                    self.calendarCmds.delete_event(event)
                elif(event.get_uid() == uid2):
                    end_event_found = True
                    # Delete the end event
                    self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(start_event_found and end_event_found)

    ###
    # Test to get two events : monday 2017/06/19 and sunday 2017/06/25 between
    # 8h00 and 10h00.
    ###
    def test_get_events_week(self):
        start_event_found = False
        end_event_found = False

        summary1 = "test_get_events_week start"
        summary2 = "test_get_events_week end"

        date1 = "20170619"
        start_date1 = DateUtils.get_datetime(
                "{0}T080000Z".format(date1))
        end_date1 = DateUtils.get_datetime(
                "{0}T100000Z".format(date1))
        date2 = "20170625"
        start_date2 = DateUtils.get_datetime(
                "{0}T080000Z".format(date2))
        end_date2 = DateUtils.get_datetime(
                "{0}T100000Z".format(date2))
        # Create one event Monday and one Sunday
        uid1 = self.calendarCmds.create_event(
                summary1,
                start_date1,
                end_date1)
        uid2 = self.calendarCmds.create_event(
                summary2,
                start_date2,
                end_date2)
        if(uid1 and uid2):
            search_day = "20170621"
            events = self.calendarCmds.get_events_week(search_day)
            for event in events:
                # Verify if the event correspond
                if(event.get_uid() == uid1):
                    start_event_found = True
                    # Delete the start event
                    self.calendarCmds.delete_event(event)
                elif(event.get_uid() == uid2):
                    end_event_found = True
                    # Delete the end event
                    self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(start_event_found and end_event_found)

    ###
    # Test to get two events : the 1st of this month and the last day of this
    # month between 8h00 and 10h00.
    ###
    def test_get_events_this_month(self):
        start_event_found = False
        end_event_found = False

        summary1 = "test_get_events_this_month start"
        summary2 = "test_get_events_this_month end"

        date1 = DateUtils.get_date_string(DateUtils.get_monthday_start())
        start_date1 = DateUtils.get_datetime(
                "{0}T080000Z".format(date1))
        end_date1 = DateUtils.get_datetime(
                "{0}T100000Z".format(date1))
        date2 = DateUtils.get_date_string(DateUtils.get_monthday_end())
        start_date2 = DateUtils.get_datetime(
                "{0}T080000Z".format(date2))
        end_date2 = DateUtils.get_datetime(
                "{0}T100000Z".format(date2))
        # Create two events
        uid1 = self.calendarCmds.create_event(
                summary1,
                start_date1,
                end_date1)
        uid2 = self.calendarCmds.create_event(
                summary2,
                start_date2,
                end_date2)
        if(uid1 and uid2):
            events = self.calendarCmds.get_events_month()
            for event in events:
                # Verify if the event correspond
                if(event.get_uid() == uid1):
                    start_event_found = True
                    # Delete the start event
                    self.calendarCmds.delete_event(event)
                elif(event.get_uid() == uid2):
                    end_event_found = True
                    # Delete the end event
                    self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(start_event_found and end_event_found)

    ###
    # Test to get two events : the 2016/02/01 and the 2016/02/29 between 8h00
    # and 10h00.
    ###
    def test_get_events_month(self):
        start_event_found = False
        end_event_found = False

        summary1 = "test_get_events_month start"
        summary2 = "test_get_events_month end"

        date1 = "20160201"
        start_date1 = DateUtils.get_datetime(
                "{0}T080000Z".format(date1))
        end_date1 = DateUtils.get_datetime(
                "{0}T100000Z".format(date1))
        date2 = "20160229"
        start_date2 = DateUtils.get_datetime(
                "{0}T080000Z".format(date2))
        end_date2 = DateUtils.get_datetime(
                "{0}T100000Z".format(date2))
        # Create two events
        uid1 = self.calendarCmds.create_event(
                summary1,
                start_date1,
                end_date1)
        uid2 = self.calendarCmds.create_event(
                summary2,
                start_date2,
                end_date2)
        if(uid1 and uid2):
            search_day = "20160208"
            events = self.calendarCmds.get_events_month(search_day)
            for event in events:
                # Verify if the event correspond
                if(event.get_uid() == uid1):
                    start_event_found = True
                    # Delete the start event
                    self.calendarCmds.delete_event(event)
                elif(event.get_uid() == uid2):
                    end_event_found = True
                    # Delete the end event
                    self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(start_event_found and end_event_found)

    ###
    # Test to get two events created with one recursive event with end date:
    # the 2017/07/11 and the 2017/07/18 between 22h00 and 23h00.
    ###
    def test_get_recursive_end_date_events_month(self):
        start_event_found = False
        end_event_found = False

        summary = "test_get_recursive_end_date_events_month"

        date = "20170711"
        start_date = DateUtils.get_datetime(
                "{0}T220000Z".format(date))
        end_date = DateUtils.get_datetime(
                "{0}T230000Z".format(date))

        first_start_date = DateUtils.get_datetime("20170711T220000Z")
        second_start_date = DateUtils.get_datetime("20170718T220000Z")

        recurrence_frequency = "WEEKLY"
        recurrence_interval = 1
        recurrence_end_date_str = "20170718"
        recurrence_end_date = DateUtils.get_datetime(
                "{0}T235959Z".format(recurrence_end_date_str))

        # Create one recursive event
        uid = self.calendarCmds.create_event(
                summary,
                start_date,
                end_date,
                recurrence_frequency = recurrence_frequency,
                recurrence_interval = recurrence_interval,
                recurrence_end_date = recurrence_end_date)

        if(uid):
            search_day = "20170723"
            events = self.calendarCmds.get_events_month(search_day)
            for event in events:
                # Verify if the event correspond
                if(event.get_uid() == uid
                        and event.get_start() == first_start_date):
                    start_event_found = True
                    # Delete the event if all found
                    if(end_event_found):
                        self.calendarCmds.delete_event(event)
                elif(event.get_uid() == uid
                        and event.get_start() == second_start_date):
                    end_event_found = True
                    # Delete the event if all found
                    if(start_event_found):
                        self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(start_event_found and end_event_found)

    ###
    # Test to get two events created with one recursive event with max count:
    # the 2017/07/11 and the 2017/07/18 between 22h00 and 23h00.
    ###
    def test_get_recursive_count_events_month(self):
        start_event_found = False
        end_event_found = False

        summary = "test_get_recursive_count_events_month"

        date = "20170711"
        start_date = DateUtils.get_datetime(
                "{0}T220000Z".format(date))
        end_date = DateUtils.get_datetime(
                "{0}T230000Z".format(date))

        first_start_date = DateUtils.get_datetime("20170711T220000Z")
        second_start_date = DateUtils.get_datetime("20170718T220000Z")

        recurrence_frequency = "WEEKLY"
        recurrence_interval = 1
        recurrence_count = 2

        # Create one recursive event
        uid = self.calendarCmds.create_event(
                summary,
                start_date,
                end_date,
                recurrence_frequency = recurrence_frequency,
                recurrence_interval = recurrence_interval,
                recurrence_count = recurrence_count)

        if(uid):
            search_day = "20170723"
            events = self.calendarCmds.get_events_month(search_day)
            for event in events:
                # Verify if the event correspond
                if(event.get_uid() == uid
                        and event.get_start() == first_start_date):
                    start_event_found = True
                    # Delete the event if all found
                    if(end_event_found):
                        self.calendarCmds.delete_event(event)
                elif(event.get_uid() == uid
                        and event.get_start() == second_start_date):
                    end_event_found = True
                    # Delete the event if all found
                    if(start_event_found):
                        self.calendarCmds.delete_event(event)

        # Return res
        self.assertTrue(start_event_found and end_event_found)

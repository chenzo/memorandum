# Memorandum
Command line tool to display calendar in terminal, mutt, etc.

# Install
```bash
pip3 install memorandum --user
```
# First run and auto-configuration
```bash
python3 -m memorandum
```
Answer configuration questions and it will install :
  * a configuration file in "~/.config/memorandum/memorandum.rc"
  * and if you wish an "memorandum" alias in "~/.bash_aliases".

# Usage

## Run
```bash
memorandum
```
N.B.: this command is only available on a new shell after the first run
    instructions and alias configuration are done (c.f. section above).

## Options
The main options are:
  * `memorandum`: display today events
  * `memorandum -w`: display this week events
  * `memorandum -m`: display this month events
  * `memorandum -c`: create a new event
  * `memorandum -d YYYYMMDD`: display the corresponding day events
  * `memorandum -w -d YYYYMMDD`: display the corresponding week events
  * `memorandum -m -d YYYYMMDD`: display the corresponding month events
  * `memorandum -c -d YYYYMMDD`: create a new event for a specific date

# TODO: Mutt usage

## TODO: Add to .mailcap file:
text/calendar; path/memorandum/cal.py '%s'; nametemplate=%s.ics; copiousoutput;

## TODO: Add to .muttrc file:
auto_view text/calendar

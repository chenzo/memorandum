#!/usr/bin/python3

###
# Author: Vincent Lucas <vincent.lucas@gmail.com>
###

import sys
import vobject

from memorandum.CalDavSession import *
from memorandum.VCalendar import *
from memorandum.utils import *

import os
from os.path import isfile, join

###
# Parse an .ics and display the summary and dates.
#
# @param filename An .ics filename containing an or several events.
###
if __name__ == "__main__":
    # Parse the command line
    if(len(sys.argv) != 2):
        print("Usage: " + sys.argv[0] + " <ics_directory>")
        sys.exit(1)

    directory = sys.argv[1]

    # Initialize the valendar with a local file.
    vcalendar = VCalendar()

    for f in os.listdir(directory):
        filename_path = join(directory, f)
        filename_path_dst = join(directory + "done/", f)
        if isfile(filename_path):
            print("\n\n----\n\n----\n\nfile: {0}".format(filename_path))
            vcalendar.read(filename_path)
            # Pretty print the vcalendar for debug purpose.
            vcalendar.pretty_print()

            vevents = vcalendar.get_vevents()
            for vevent in vevents:
                #vobject.VEvent().validate(vevent)
                vevent.validate(vevent)
            os.rename(filename_path, filename_path_dst)

    sys.exit(0)
